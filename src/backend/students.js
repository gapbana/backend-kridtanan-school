'use strict';
const pool = require('../db');

const getStudentList = async () => {
	let [rows] = await pool.query(`
    SELECT *
    FROM students
    `);

	return rows;
};

const getStudentById = async id => {
	let [rows] = await pool.query(
		`
    SELECT * 
    FROM students
    WHERE students.id = ?`,
		[id]
	);

	return rows;
};

const getStudentCourseById = async id => {
	let [rows] = await pool.query(
		`
    SELECT students.id as std_id , students.name as std_name , email 
    , phone , dob , students.created_at as std_create , 
    students.updated_at as std_update ,courses.id as c_id , 
    courses.name as c_name , detail , duration ,
    courses.created_at as c_create , courses.updated_at as c_update
    FROM students , courses
    WHERE students.id = ?
    `,
		[id]
	);

	return rows;
};

const getStudentAssessmentById = async (sId, cId) => {
	let [rows] = await pool.query(
		`
    SELECT assessments.score as score , assessments.updated_at as at
    FROM enrolls , assessments
    WHERE enrolls.student_id = assessments.student_id 
    AND assessments.student_id = ? 
    AND enrolls.course_id = ?
    AND assessments.course_id = ?
    `,
		[sId, cId, cId]
	);

	return rows;
};

const register = async data => {
	let [rows] = await pool.query(
		`
    INSERT INTO students
    (name , email , phone ,dob)
    VALUES 
    (?,?,?,?)`,
		[data.name, data.email, data.phone, data.dob]
	);

	return rows;
};

const getAllStudentAssesmentScore = async () => {
	let [rows] = await pool.query(`
    SELECT students.id , students.name , students.phone , courses.name as course_name, assessments.score 
        , (SELECT AVG(score) 
	    FROM assessments
	    WHERE students.id = assessments.student_id ) as average
    FROM students , assessments , courses ,enrolls
    WHERE students.id = enrolls.student_id = assessments.student_id
    AND assessments.course_id = courses.id
    ORDER BY students.id 
`);

    return rows
};

module.exports = {
	getStudentList,
	getStudentById,
	getStudentCourseById,
	getStudentAssessmentById,
    register,
    getAllStudentAssesmentScore
};
