const Router = require('koa-router')

const router = new Router()


const home = require('./home')


router.get("/", home.getHandle)
router.get("/students", home.getAllStudent)
router.get("/students/:id", home.getStudentById)
router.get("/students/:id/courses",home.getStudentCourse)
router.get("/students/:sId/courses/:cId",home.getStudentAssesment)
router.get("/score",home.getAvgScore)
router.post("/register",home.register)





module.exports = router.routes()