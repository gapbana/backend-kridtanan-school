'use strict'
const Koa = require('koa');
const koaBody = require('koa-body')
const Router = require('koa-router');
const session = require('koa-session')
const serve = require('koa-static');
const path = require('path');
const render = require('koa-ejs');


const app = new Koa();
const router = new Router();
const PORT = 3001;

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
});


app.use(koaBody({
    multipart: true
}));

app.use(require('./route'))

app.use(serve('public'))

//app.use(serve(path.join(__dirname, 'public')));
// app.use(router.routes());
// app.use(router.allowedMethods());

app.listen(PORT , () =>
        console.log('App listening on http://localhost:'+ PORT)
)