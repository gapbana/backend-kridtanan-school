const { student } = require('../backend');
const { home } = require('../App');
const axios = require('axios');

const getHandle = async ctx => {
	//const fetchData = home.getFetchData();
	axios
		.get('http://localhost:3001/students/1')
		.then(function(response) {
			// handle success
			console.log(response.data.studentById);
		})
		.catch(function(error) {
			// handle error
			console.log(error);
		})
		.then(function() {
			// always executed
		});
	await ctx.render('home', fetchData);
	// console.log(fetchData);
};

const getAllStudent = async ctx => {
	const studentList = await student.getStudentList();
	return (ctx.body = { studentList });
};

const getStudentById = async ctx => {
	const id = ctx.params.id;
	const studentById = await student.getStudentById(id);
	return (ctx.body = { studentById });
};

const getStudentCourse = async ctx => {
	const id = ctx.params.id;
	const studentCourseById = await student.getStudentCourseById(id);
	return (ctx.body = { studentCourseById });
};

const getStudentAssesment = async ctx => {
	const sId = ctx.params.sId;
	const cId = ctx.params.cId;
	const assessmentById = await student.getStudentAssessmentById(sId, cId);
	return (ctx.body = { assessmentById });
};

const register = async ctx => {
	//const { name, email, phone, dob } = ctx.request.body;

	let data = {
		name,
		email,
		phone,
		dob,
	};

	await student.register(data);
};

const getAvgScore = async ctx => {
	const studentScore = await student.getAllStudentAssesmentScore()
	return (ctx.body = { studentScore });
}

module.exports = {
	getAllStudent,
	getStudentById,
	getStudentCourse,
	getStudentAssesment,
	register,
	getHandle,
	getAvgScore
};
